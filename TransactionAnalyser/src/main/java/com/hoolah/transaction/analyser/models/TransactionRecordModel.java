package com.hoolah.transaction.analyser.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@ToString
public class TransactionRecordModel {

	private String transactionId;
	private String transactionDate;
	private String transactionAmount;
	private String merchantName;
	private String transactionType;
	private String transactionReversalId;

}
