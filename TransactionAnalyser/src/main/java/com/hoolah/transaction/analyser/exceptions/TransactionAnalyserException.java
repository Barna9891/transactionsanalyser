package com.hoolah.transaction.analyser.exceptions;

public class TransactionAnalyserException extends Exception {

	private static final long serialVersionUID = -2083246709525607623L;

	public TransactionAnalyserException() {
		super();
	}

	public TransactionAnalyserException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public TransactionAnalyserException(String message, Throwable cause) {
		super(message, cause);
	}

	public TransactionAnalyserException(String message) {
		super(message);
	}

	public TransactionAnalyserException(Throwable cause) {
		super(cause);
	}
	
}
