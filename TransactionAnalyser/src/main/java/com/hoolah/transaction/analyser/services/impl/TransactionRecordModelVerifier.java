package com.hoolah.transaction.analyser.services.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hoolah.transaction.analyser.TransactionAnalyser;
import com.hoolah.transaction.analyser.models.TransactionRecordModel;
import com.hoolah.transaction.analyser.services.ModelVerifier;
import com.hoolah.transaction.analyser.utilities.Utility;

public class TransactionRecordModelVerifier implements ModelVerifier {
	
	private static volatile TransactionRecordModelVerifier modelVerifier=null;
	
	private static final Logger logger = LoggerFactory.getLogger(TransactionAnalyser.class);
	
	private TransactionRecordModelVerifier() {
		
	}

	public static synchronized TransactionRecordModelVerifier getInstance() {
		if(modelVerifier==null) {
			modelVerifier = new TransactionRecordModelVerifier();
		}
		return modelVerifier;
	}
	
	public boolean verifyModel(TransactionRecordModel transactionRecordModel) {

		if (Utility.isBlank(transactionRecordModel.getTransactionId())) {
			logger.error("Transaction ID should not be Empty");
			return false;
		}

		if (Utility.isBlank(transactionRecordModel.getTransactionDate())) {
			logger.error("Transaction Date should not be Empty");
			return false;
		}

		if (Utility.isBlank(transactionRecordModel.getMerchantName())) {
			logger.error("Merchant Name should not be Empty");
			return false;
		}

		if (Utility.isBlank(transactionRecordModel.getTransactionAmount())) {
			logger.error("Transaction Amount should not be Empty");
			return false;
		}

		if (Utility.isBlank(transactionRecordModel.getTransactionType())) {
			logger.error("Transaction Type should not be Empty");
			return false;
		}
		return true;
	}

}
