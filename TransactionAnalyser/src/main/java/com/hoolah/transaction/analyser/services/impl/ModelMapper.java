package com.hoolah.transaction.analyser.services.impl;

import org.apache.commons.csv.CSVRecord;

import com.hoolah.transaction.analyser.models.TransactionRecordModel;

public class ModelMapper {

	public static TransactionRecordModel generateTransactionRecordModel(CSVRecord csvRecord) {		
		return new TransactionRecordModel(csvRecord.get(0), csvRecord.get(1), csvRecord.get(2), csvRecord.get(3), csvRecord.get(4), csvRecord.get(5));		
	}
	
}
