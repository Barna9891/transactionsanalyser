package com.hoolah.transaction.analyser.services;

import com.hoolah.transaction.analyser.models.TransactionRecordModel;

public interface ModelVerifier {

	public boolean verifyModel(TransactionRecordModel transactionRecordModel);
	
}
