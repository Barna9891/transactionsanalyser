package com.hoolah.transaction.analyser.services.impl;

import java.util.Iterator;
import java.util.Map;

import com.hoolah.transaction.analyser.exceptions.TransactionAnalyserException;
import com.hoolah.transaction.analyser.models.TransactionRecordModel;
import com.hoolah.transaction.analyser.services.Computations;
import com.hoolah.transaction.analyser.utilities.Utility;

public class TransactionsAverageComputation implements Computations{

	private static volatile TransactionsAverageComputation transactionsAverageComputation=null;
	
	private TransactionsAverageComputation() {
		
	}
	
	public static synchronized TransactionsAverageComputation getInstance() {
		if(transactionsAverageComputation==null) {
			transactionsAverageComputation = new TransactionsAverageComputation();
		}
		return transactionsAverageComputation;
	}
	
	@Override
	public double computeTransactionsAverage(Map<String, TransactionRecordModel> finalList) throws TransactionAnalyserException {

		Iterator<TransactionRecordModel> iter = finalList.values().iterator();
		double sum = 0;
		while(iter.hasNext()) {
			sum=sum+Utility.parseDouble(iter.next().getTransactionAmount());
		}
		return sum/finalList.size();
	}

	
	
}
