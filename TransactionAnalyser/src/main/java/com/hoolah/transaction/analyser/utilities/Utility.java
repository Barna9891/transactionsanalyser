package com.hoolah.transaction.analyser.utilities;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.hoolah.transaction.analyser.exceptions.TransactionAnalyserException;
import com.hoolah.transaction.analyser.models.TransactionRecordModel;

public final class Utility {

	private Utility() {
		throw new UnsupportedOperationException();
	}

	public static boolean isBlank(final String str) {
		return str==null||str.trim().length()==0;
	}

	public static Reader getCsvFileReader(final String fileName) throws IOException {
		return new BufferedReader(new FileReader(fileName));
	}

	public static boolean isMatches(final String pattern, final String matchText) {
		Pattern p = Pattern.compile(pattern);
		Matcher m = p.matcher(matchText);
		return m.matches();
	}

	public static Date getDateFromString(final String pattern, final String dateString) throws TransactionAnalyserException {
		try {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
			return simpleDateFormat.parse(dateString);
		} catch (ParseException e) {
			throw new TransactionAnalyserException("Exeception while parsing Date " + dateString, e);
		}
	}
	
	public static boolean checkIfDateInRange(TransactionRecordModel trm, Date start, Date end) {
		DateFormat format = new SimpleDateFormat(Constants.DATE_PATTERN);		
		try {
			return format.parse(trm.getTransactionDate()).after(start) && format.parse(trm.getTransactionDate()).before(end);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public static Double parseDouble(final String longString) throws TransactionAnalyserException {
		try {
			return Double.parseDouble(longString);
		} catch (NumberFormatException e) {
			throw new TransactionAnalyserException("Exeception while parsing Double " + longString, e);
		}
	}
	
}

