package com.hoolah.transaction.analyser.services;

import java.util.List;

import com.hoolah.transaction.analyser.exceptions.TransactionAnalyserException;
import com.hoolah.transaction.analyser.models.TransactionRecordModel;

public interface TransactionFileLoader {
	
	public List<TransactionRecordModel> load(final String fileName) throws TransactionAnalyserException;

}
