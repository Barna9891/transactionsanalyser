package com.hoolah.transaction.analyser;

import java.util.Map;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hoolah.transaction.analyser.exceptions.TransactionAnalyserException;
import com.hoolah.transaction.analyser.models.TransactionRecordModel;
import com.hoolah.transaction.analyser.services.Computations;
import com.hoolah.transaction.analyser.services.TransactionProcessor;
import com.hoolah.transaction.analyser.services.impl.CsvTransactionProcessor;
import com.hoolah.transaction.analyser.services.impl.TransactionsAverageComputation;
import com.hoolah.transaction.analyser.utilities.Constants;
import com.hoolah.transaction.analyser.utilities.Utility;

public class TransactionAnalyser {

	private static final Logger logger = LoggerFactory.getLogger(TransactionAnalyser.class);

	public static void main(String[] args) {
		TransactionProcessor transactionProcessor = CsvTransactionProcessor.getInstance();
		Computations transactionsAverageComputation = TransactionsAverageComputation.getInstance();
		logger.info("[Mandatory Field] - Please enter the csv file path");
		Scanner in = new Scanner(System.in);
		String fileName = in.nextLine();
		logger.info("[Mandatory Field] - Please enter the merchant name");
		String merchantName = in.nextLine();
		logger.info("[Mandatory Field] - Please enter the start date of the range");
		String startDate = in.nextLine();
		logger.info("[Mandatory Field] - Please enter the end date of the range");
		String endDate = in.nextLine();
		in.close();
		if (Utility.isBlank(fileName) || Utility.isBlank(merchantName) || Utility.isBlank(startDate)	|| Utility.isBlank(endDate)) {
			logger.info("One of the fields is empty and hence taking default values for all the fields");
			fileName = "src\\main\\resources\\transactions.csv";
			merchantName = "Kwik-E-Mart";
			startDate = "20/08/2018 12:00:00";
			endDate = "20/08/2018 13:00:00";
		}
		try {
			Map<String, TransactionRecordModel> finalList = transactionProcessor.process(fileName, merchantName,
					Utility.getDateFromString(Constants.DATE_PATTERN, startDate),
					Utility.getDateFromString(Constants.DATE_PATTERN, endDate));
			logger.info("Number of Transactions = " + finalList.size());
			logger.info("Average Transaction Value = " + transactionsAverageComputation.computeTransactionsAverage(finalList));
		} catch (TransactionAnalyserException e) {
			logger.error("Exception caught : ", e);
		}
	}

}
