package com.hoolah.transaction.analyser.services.impl;

import java.io.IOException;
import java.io.Reader;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;

import com.hoolah.transaction.analyser.exceptions.TransactionAnalyserException;
import com.hoolah.transaction.analyser.models.TransactionRecordModel;
import com.hoolah.transaction.analyser.services.TransactionFileLoader;
import com.hoolah.transaction.analyser.utilities.Utility;

public class CsvTransactionFileLoader implements TransactionFileLoader {
	
	private static volatile CsvTransactionFileLoader csvTransactionFileLoader=null;
	
	private CsvTransactionFileLoader() {
		
	}
	
	public static synchronized CsvTransactionFileLoader getInstance() {
		if(csvTransactionFileLoader==null) {
			csvTransactionFileLoader = new CsvTransactionFileLoader();
		}
		return csvTransactionFileLoader;
	}
	
	public List<TransactionRecordModel> load(String fileName) throws TransactionAnalyserException {
		List<TransactionRecordModel> trmList;
		try(
			Reader reader = Utility.getCsvFileReader(fileName);
			CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());
			) {
			trmList = csvParser.getRecords().stream().map(ModelMapper::generateTransactionRecordModel).collect(Collectors.toList());			
		} catch (IOException e) {
			throw new TransactionAnalyserException("Exeception while the reading file : " + fileName, e);
		}
		return trmList;
	}
	
}
