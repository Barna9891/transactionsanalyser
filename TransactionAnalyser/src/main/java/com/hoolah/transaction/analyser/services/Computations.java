package com.hoolah.transaction.analyser.services;

import java.util.Map;

import com.hoolah.transaction.analyser.exceptions.TransactionAnalyserException;
import com.hoolah.transaction.analyser.models.TransactionRecordModel;

public interface Computations {

	public double computeTransactionsAverage(Map<String,TransactionRecordModel> finalList) throws TransactionAnalyserException;
	
}
