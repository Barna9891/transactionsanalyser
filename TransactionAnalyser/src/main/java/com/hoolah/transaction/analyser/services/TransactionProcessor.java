package com.hoolah.transaction.analyser.services;

import java.util.Date;
import java.util.Map;

import com.hoolah.transaction.analyser.exceptions.TransactionAnalyserException;
import com.hoolah.transaction.analyser.models.TransactionRecordModel;

public interface TransactionProcessor {

	public Map<String, TransactionRecordModel> process(String fileName, String merchantName, Date start, Date end) throws TransactionAnalyserException;
	
}
