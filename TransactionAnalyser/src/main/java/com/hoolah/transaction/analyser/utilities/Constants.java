package com.hoolah.transaction.analyser.utilities;

public final class Constants {

	private Constants() {
		throw new UnsupportedOperationException();
	}

	public static final String DATE_PATTERN = "dd/MM/yyyy HH:mm:ss";
	
	public static final String REVERSAL = "REVERSAL";
	
	public static final String PAYMENT = "PAYMENT";
}
