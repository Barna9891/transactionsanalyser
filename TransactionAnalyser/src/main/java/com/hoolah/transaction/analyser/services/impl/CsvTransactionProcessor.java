package com.hoolah.transaction.analyser.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.hoolah.transaction.analyser.exceptions.TransactionAnalyserException;
import com.hoolah.transaction.analyser.models.TransactionRecordModel;
import com.hoolah.transaction.analyser.services.ModelVerifier;
import com.hoolah.transaction.analyser.services.TransactionFileLoader;
import com.hoolah.transaction.analyser.services.TransactionProcessor;
import com.hoolah.transaction.analyser.utilities.Constants;
import com.hoolah.transaction.analyser.utilities.Utility;

public class CsvTransactionProcessor implements TransactionProcessor {

	private TransactionFileLoader csvTransactionFileLoader = CsvTransactionFileLoader.getInstance(); 
	private ModelVerifier modelVerifier = TransactionRecordModelVerifier.getInstance();	
	
	private static volatile CsvTransactionProcessor csvTransactionProcessor=null;
	
	private CsvTransactionProcessor() {
		
	}
	
	public static synchronized CsvTransactionProcessor getInstance() {
		if(csvTransactionProcessor==null) {
			csvTransactionProcessor = new CsvTransactionProcessor();
		}
		return csvTransactionProcessor;
	}
	
	@Override
	public Map<String,TransactionRecordModel> process(String fileName,String merchantName, Date start, Date end) throws TransactionAnalyserException {
		List<TransactionRecordModel> reversalList = new ArrayList<>();
		Map<String,TransactionRecordModel> paymentList = csvTransactionFileLoader.load(fileName)
				.stream()
				.filter(p->modelVerifier.verifyModel(p))
				.filter(p->p.getMerchantName().equals(merchantName))
				.map(p->{
					if(p.getTransactionType().equals(Constants.REVERSAL)) {
						reversalList.add(p);
					}
					return p;
					})
				.filter(p->p.getTransactionType().equals(Constants.PAYMENT))
				.filter(p->Utility.checkIfDateInRange(p, start, end))
				.collect(Collectors.toMap(p->p.getTransactionId(),p->p));
		reversalList.forEach(p->paymentList.remove(p.getTransactionReversalId()));
		return paymentList;
	}
	
}
