package com.hoolah.transaction.analyser.test;

import static org.junit.Assert.assertEquals;

import java.util.Map;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hoolah.transaction.analyser.TransactionAnalyser;
import com.hoolah.transaction.analyser.exceptions.TransactionAnalyserException;
import com.hoolah.transaction.analyser.models.TransactionRecordModel;
import com.hoolah.transaction.analyser.services.TransactionProcessor;
import com.hoolah.transaction.analyser.services.impl.CsvTransactionProcessor;
import com.hoolah.transaction.analyser.services.impl.TransactionsAverageComputation;
import com.hoolah.transaction.analyser.utilities.Constants;
import com.hoolah.transaction.analyser.utilities.Utility;

public class TransactionAnalyserTest {

	private static final Logger logger = LoggerFactory.getLogger(TransactionAnalyser.class);
	
	private static TransactionProcessor transactionProcessor = CsvTransactionProcessor.getInstance();
	private static TransactionsAverageComputation transactionsAverageComputation = TransactionsAverageComputation.getInstance();
	
	@Test
	public void test_transactions_for_Kwik_E_Mart_between_12_to_13_hours() {
		try {
			Map<String,TransactionRecordModel> finalList = transactionProcessor.process("src\\test\\resources\\test-transactions.csv", "Kwik-E-Mart",
					Utility.getDateFromString(Constants.DATE_PATTERN,"20/08/2018 12:00:00"), Utility.getDateFromString(Constants.DATE_PATTERN,"20/08/2018 13:00:00"));
			assertEquals("59.99",Double.toString(transactionsAverageComputation.computeTransactionsAverage(finalList)));
		} catch (TransactionAnalyserException e) {
			logger.error("Exception caught : ", e);
		}		
	}
	
	@Test
	public void test_transactions_for_MacLaren_between_12_to_15_hours() {
		try {
			Map<String,TransactionRecordModel> finalList = transactionProcessor.process("src\\test\\resources\\test-transactions.csv", "MacLaren",
					Utility.getDateFromString(Constants.DATE_PATTERN,"20/08/2018 12:00:00"), Utility.getDateFromString(Constants.DATE_PATTERN,"20/08/2018 15:00:00"));
			assertEquals("52.25",Double.toString(transactionsAverageComputation.computeTransactionsAverage(finalList)));
		} catch (TransactionAnalyserException e) {
			logger.error("Exception caught : ", e);
		}		
	}
	
	

}
